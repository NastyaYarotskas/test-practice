package com.netcracker.test;

import com.netcracker.yarotskas.dao.PersonDao;
import com.netcracker.yarotskas.dto.Employer;
import com.netcracker.yarotskas.entity.Person;
import com.netcracker.yarotskas.exception.NotPersonException;
import com.netcracker.yarotskas.exception.PersonNotFoundException;
import com.netcracker.yarotskas.service.DefaultPersonService;
import com.netcracker.yarotskas.service.PersonService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {

    @InjectMocks
    private PersonService personService = new DefaultPersonService();

    @Mock
    private PersonDao personDao;

    private Employer employer;
    private Person person;
    private String id;
    private UUID uuid;

    private List<Employer> employerList;
    private List<Person> personList;
    private List<String> idList;
    private List<UUID> uuidList;

    @BeforeEach
    public void init() {
        employer = new Employer("Dima", "Pushkin", 30);

        uuid = UUID.randomUUID();
        id = UUID.randomUUID().toString();

        employerList = new LinkedList<>();
        personList = new LinkedList<>();
        idList = new LinkedList<>();
        uuidList = new LinkedList<>();

        for (int i = 0; i < 6; i++) {
            idList.add(UUID.randomUUID().toString());
            uuidList.add(UUID.randomUUID());
        }

        employerList.add(new Employer("Nadine", "Cruz", 35));
        employerList.add(new Employer("Ryan", "Harrington", 23));
        employerList.add(new Employer("Roosevelt", "Tran", 24));
        employerList.add(new Employer("Ira", "Richardson", 12));
        employerList.add(new Employer("Devin", "Guerrero", 34));
        employerList.add(new Employer("Norman", "Roberson", 56));

        personList.add(new Person("Nadine", (byte) 35));
        personList.add(new Person("Nadine", (byte) 35));
        personList.add(new Person("Nadine", (byte) 35));
        personList.add(new Person("Nadine", (byte) 35));
        personList.add(new Person("Nadine", (byte) 35));
        personList.add(new Person("Nadine", (byte) 35));

        person = new Person("Alex", (byte) 25);
    }

    @Test
    public void save() {
        when(personDao.savePerson(any(Person.class))).thenReturn(id);
        UUID uuid = personService.save(employer);
        assertNotNull(uuid);
    }

    @Test
    public void get() throws NotPersonException, PersonNotFoundException {
        when(personDao.getPerson(uuid.toString())).thenReturn(person);
        Person person = personService.get(uuid);
        assertNotNull(person);
    }

    @Test
    public void getException() throws NotPersonException, PersonNotFoundException {
        when(personDao.getPerson(uuid.toString())).thenThrow(new NotPersonException());
        Person person = personService.get(uuid);
        assertNull(person);
    }

    @Test
    public void saveAll() {
        List<UUID> uuidList = personService.saveAll(employerList);
        assertNotNull(uuidList);
    }

    @Test
    public void findAll() {
        List<Person> personList = personService.findAll((employerList));
        assertNotNull(personList);
    }

    @Test
    public void findByName() throws NotPersonException, PersonNotFoundException {
        when(personDao.findByName(any(String.class))).thenReturn(personList);
        List<Person> personList = personService.findByName("Nadine");
        assertNotNull(personList);
    }

    @Test
    public void findByNameException() throws NotPersonException, PersonNotFoundException {
        when(personDao.findByName(any(String.class))).thenThrow(new NotPersonException());
        List<Person> personList = personService.findByName("Nadine");
        assertNull(personList);
    }

    @Test
    public void findByAge() throws NotPersonException, PersonNotFoundException {
        when(personDao.findByAge(any(Integer.class))).thenReturn(personList);
        List<Person> personList = personService.findByAge(23);
        assertNotNull(personList);
    }

    @Test
    public void findByAgeException() throws NotPersonException, PersonNotFoundException {
        when(personDao.findByAge(any(Integer.class))).thenThrow(new NotPersonException());
        List<Person> personList = personService.findByAge(23);
        assertNull(personList);
    }

    @Test
    public void findByNameAndAge() throws NotPersonException, PersonNotFoundException {
        when(personDao.findByNameAndAge(any(String.class), any(Integer.class))).thenReturn(personList);
        List<Person> personList = personService.findByNameAndAge("Nina", 23);
        assertNotNull(personList);
    }

    @Test
    public void findByNameAndAgeException() throws NotPersonException, PersonNotFoundException {
        when(personDao.findByNameAndAge(any(String.class), any(Integer.class))).thenThrow(new PersonNotFoundException());
        List<Person> personList = personService.findByNameAndAge("Nina", 23);
        assertNull(personList);
    }

    @Test
    public void deleteById() throws PersonNotFoundException, NotPersonException {
        personDao.deleteById("");
        assertNotNull(personService.getAll());
    }

    @Test
    public void deleteByName() {
        personService.deleteByName("Kate");
        assertNotNull(personService.getAll());
    }

    @Test
    public void deleteAll() {
        personService.deleteAll();
        assertEquals(0, personService.getAll().size());
    }
}
