package com.netcracker.yarotskas.dto;

import lombok.Data;

@Data
public class Employer {

    private String name;

    private String surname;

    private int age;

    public Employer(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
}
