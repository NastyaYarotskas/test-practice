package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dao.DefaultPersonDao;
import com.netcracker.yarotskas.dao.PersonDao;
import com.netcracker.yarotskas.dto.Employer;
import com.netcracker.yarotskas.entity.Person;
import com.netcracker.yarotskas.exception.NotPersonException;
import com.netcracker.yarotskas.exception.PersonNotFoundException;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class DefaultPersonService implements PersonService {

    PersonDao personDao = new DefaultPersonDao();

    @Override
    public UUID save(Employer employer) {
        Person person = createPerson(employer);
        String id = personDao.savePerson(person);
        return UUID.fromString(id);
    }

    private Person createPerson(Employer employer) {
        return new Person(employer.getName(), (byte) employer.getAge());
    }

    @Override
    public Person get(UUID uuid) {
        Person person = null;
        try {
            person = personDao.getPerson(uuid.toString());
        } catch (PersonNotFoundException | NotPersonException e) {
            System.err.println("ERROR");
        }
        return person;
    }

    @Override
    public List<UUID> saveAll(List<Employer> employersList) {
//        List<Person> persons = new LinkedList<>();
//
//        for (Employer employer : employersList) {
//            persons.add(createPerson(employer));
//        }
//
//        List<String> idList = personDao.saveAllPersons(persons);
//        List<UUID> uuidList = new LinkedList<>();
//        for (String id : idList) {
//            uuidList.add(UUID.fromString(id));
//        }

        List<UUID> uuidList = new LinkedList<>();
        for(Employer employer : employersList) {
            uuidList.add(save(employer));
        }

        return uuidList;
    }

    @Override
    public List<Person> findAll(List<Employer> employersList) {
        List<Person> result = new LinkedList<>();
        for (Employer employer : employersList) {
            List<Person> personList = findByNameAndAge(employer.getName(), employer.getAge());
            if(!personList.isEmpty()) {
                result.addAll(personList);
            }
        }

        return result;
    }

    @Override
    public List<Person> getAll() {
        try {
            return personDao.getAllPersons();
        } catch (NotPersonException e) {
            System.err.println("ERROR");
        } catch (PersonNotFoundException e) {
            System.err.println("ERROR");
        }

        return null;
    }

    @Override
    public List<Person> findByName(String name) {
        try {
            return personDao.findByName(name);
        } catch (NotPersonException e) {
            System.err.println("ERROR");
        } catch (PersonNotFoundException e) {
            System.err.println("ERROR");
        }

        return  null;
    }

    @Override
    public List<Person> findByAge(Integer age) {
        try {
            return personDao.findByAge(age);
        } catch (NotPersonException e) {
            System.err.println("ERROR");
        } catch (PersonNotFoundException e) {
            System.err.println("ERROR");
        }

        return null;
    }

    @Override
    public void deleteById(UUID uuid) {
        try {
            personDao.deleteById(uuid.toString());
        } catch (NotPersonException e) {
            System.err.println("ERROR");
        } catch (PersonNotFoundException e) {
            System.err.println("ERROR");
        }
    }

    @Override
    public void deleteByName(String name) {
        try {
            personDao.deleteByName(name);
        } catch (PersonNotFoundException e) {
            System.err.println("ERROR");
        } catch (NotPersonException e) {
            System.err.println("ERROR");
        }

    }

    @Override
    public void deleteAll() {
        personDao.deleteAll();
    }

    @Override
    public List<Person> findByNameAndAge(String name, Integer age) {
        try {
            return personDao.findByNameAndAge(name, age);
        } catch (NotPersonException e) {
            System.err.println("ERROR");
        } catch (PersonNotFoundException e) {
            System.err.println("ERROR");
        }

        return null;
    }
}
