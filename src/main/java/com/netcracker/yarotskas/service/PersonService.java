package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.Employer;
import com.netcracker.yarotskas.entity.Person;
import com.netcracker.yarotskas.exception.NotPersonException;
import com.netcracker.yarotskas.exception.PersonNotFoundException;

import java.util.List;
import java.util.UUID;

public interface PersonService {

    UUID save(Employer employer);

    Person get(UUID uuid);

    List<UUID> saveAll(List<Employer> employersList);

    List<Person> findAll(List<Employer> employersList);

    List<Person> getAll();

    List<Person> findByName(String name);

    List<Person> findByAge(Integer age);

    void deleteById(UUID uuid);

    void deleteByName(String name);

    void deleteAll();

    List<Person> findByNameAndAge(String name, Integer age);

}
