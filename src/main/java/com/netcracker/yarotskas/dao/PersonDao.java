package com.netcracker.yarotskas.dao;


import com.netcracker.yarotskas.entity.Person;
import com.netcracker.yarotskas.exception.NotPersonException;
import com.netcracker.yarotskas.exception.PersonNotFoundException;

import java.util.List;

public interface PersonDao {

    String savePerson(Person person);

    Person getPerson(String id) throws NotPersonException, PersonNotFoundException;

    List<Person> getAllPersons() throws NotPersonException, PersonNotFoundException;

    List<Person> findByName(String name) throws NotPersonException, PersonNotFoundException;

    List<Person> findByAge(Integer age) throws NotPersonException, PersonNotFoundException;

    void deleteById(String id) throws NotPersonException, PersonNotFoundException;

    void deleteByName(String name) throws PersonNotFoundException, NotPersonException;

    void deleteAll();

    List<Person> findByNameAndAge(String name, Integer age) throws NotPersonException, PersonNotFoundException;

    List<String> saveAllPersons(List<Person> persons);
}
