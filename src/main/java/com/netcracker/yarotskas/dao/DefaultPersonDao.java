package com.netcracker.yarotskas.dao;

import com.netcracker.yarotskas.entity.Person;
import com.netcracker.yarotskas.exception.NotPersonException;
import com.netcracker.yarotskas.exception.PersonNotFoundException;
import com.netcracker.yarotskas.storage.TemporaryStorage;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class DefaultPersonDao implements PersonDao {

    private TemporaryStorage storage = TemporaryStorage.getInstance();

    public DefaultPersonDao() {
    }

    @Override
    public String savePerson(Person person) {
        String id = UUID.randomUUID().toString();
        person.setId(id);
        storage.insertEntity(id, person);
        return id;
    }

    @Override
    public Person getPerson(String id) throws NotPersonException, PersonNotFoundException {
        Object entity = storage.getEntity(id);
        checkPerson(entity);
        return (Person) entity;
    }

    @Override
    public List<Person> getAllPersons() throws NotPersonException, PersonNotFoundException{
        List<Object> personList = storage.getAllEntities();
        for (Object person : personList) {
            checkPerson(person);
        }
        return storage.getAllEntities().stream()
                    .map(entity -> (Person) entity)
                    .collect(Collectors.toList());
    }

    @Override
    public List<String> saveAllPersons(List<Person> persons) {
        return persons.stream()
                .map(person -> savePerson(person))
                .collect(Collectors.toList());
    }

    @Override
    public List<Person> findByName(String name) throws NotPersonException, PersonNotFoundException {
        return getAllPersons().stream()
                .filter(person -> person.getName().equals(name))
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<Person> findByAge(Integer age) throws NotPersonException, PersonNotFoundException {
        return getAllPersons().stream()
                .filter(person -> person.getAge() == age)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public void deleteById(String id) throws NotPersonException, PersonNotFoundException {
        for (Object o : storage.getAllEntities()) {
            checkPerson(o);
        }
        storage.deleteEntityById(id);
    }

    @Override
    public void deleteByName(String name) throws NotPersonException, PersonNotFoundException {
        List<Person> deleteList = getAllPersons().stream()
                .filter(x -> x.getName().equals(name))
                .collect(Collectors.toList());
        deleteList.stream()
                .forEach(person -> storage.deleteEntity(person));
    }

    @Override
    public void deleteAll() {
        storage.deleteAll();
    }

    @Override
    public List<Person> findByNameAndAge(String name, Integer age) throws NotPersonException, PersonNotFoundException {
        return getAllPersons().stream()
                .filter(person -> person.getName().equals(name) && person.getAge() == age)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private void checkPerson(Object entity) throws PersonNotFoundException, NotPersonException {
        if (entity == null) {
            throw new PersonNotFoundException();
        }
        if (!Person.class.equals(entity.getClass())) {
            throw new NotPersonException();
        }
    }
}
