package com.netcracker.yarotskas.storage;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TemporaryStorage {

    private Map<String, Object> storage = new HashMap<String, Object>(256);

    public TemporaryStorage() {
    }

    public Object getEntity(String id) {
        return storage.get(id);
    }

    public void insertEntity(String id, Object entity) {
        storage.put(id, entity);
    }

    public List<Object> getAllEntities() {
        return new LinkedList<>(storage.values());
    }

    public Object deleteEntityById(String id) {
        return storage.remove(id);
    }

    public void deleteEntity(Object object) {
        storage = deleteBy(obj -> obj.equals(object));
    }

    public void deleteAll() {
        storage = new HashMap<>(256);
    }

    private Map<String, Object> deleteBy(Predicate predicate) {
        return storage.entrySet()
                .stream()
                .filter(x -> !predicate.test(x.getValue()))
                .collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
    }


    public static TemporaryStorage getInstance() {
        return Holder.instance;
    }

    private static class Holder {
        private static TemporaryStorage instance = new TemporaryStorage();
    }
}
