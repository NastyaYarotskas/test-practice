package com.netcracker.yarotskas.entity;

import lombok.Data;


@Data
public class Person {

    private String id;

    private String name;

    private byte age;

    public Person(String name, byte age) {
        this.name = name;
        this.age = age;
    }

}
